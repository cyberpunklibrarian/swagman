# Swagman

### An exercise in affordable public library self-service.

Self check out machines are incredibly expensive, with some brands and models running into five figure prices. Library budgets are already stretched thin and smaller libraries often can't afford automation technology to help their patrons get the materials they want.

And that's the basis of Swagman.

Swagman is a free, open source self-check out system that's partially a proof of concept and yet a completely working and functional library circulation tool. Swagman targets off\-the\-shelf technology and hardware and uses free and open source software to offer a way forward towards automation for the library on a budget. It's built on a lightweight Linux foundation, using Apache, PHP, Python, and MariaDB to deliver a browser\-based self-checkout machine that will work on inexpensive, lightweight hardware. To this end, Swagman is targeted at, and tested upon, the Raspberry Pi single board computer. 

So rather than spend thousands on a self\-check\-out machine, a library could set up a Swagman station for less than $500 per station. The basic hardware would include:

* Raspberry Pi 4 kit
* A monitor of some kind
* Keyboard
* Mouse
* Barcode scanner

Many libraries could reuse or repurpose existing hardware to set up their Swagman stations.

Behind the scenes, Swagman is designed for the [Polaris Integrated Library System](https://www.iii.com/products/polaris-ils/). However, because it uses standard [SIP2](https://en.wikipedia.org/wiki/Standard_Interchange_Protocol), it could be modified to work with other ILS solutions. 

Swagman aims to be a system with useful features, yet unencumbered by functionality a smaller library might not be interested in. Basic statistical reports will be added to the system, and all data will be anonymous. All of the specific patron data is deleted after they finish checking out their items. So librarians can rest easy knowing that patron privacy is maintained on the device. 

If you have questions about Swagman, or you'd like to take it for a spin, feel free to get in touch. You can reach me via:

Email: cyberpunklibrarian@protonmail.com

Twitter: [@bibrarian](https://twitter.com/bibrarian)

Mastodon: [@cyberpunklibrarian@glammr.us](https://glammr.us/@cyberpunklibrarian)

---

![Initial Screen](https://i.imgur.com/llbzI1H.png)



![Checkout Screen](https://i.imgur.com/TOz1DGi.png)