#!/usr/bin/env python3

import time
import sys
import telnetlib
import shutil
import os

# Set up transaction date and time for checkin message
trans_date = str(time.strftime('%Y%m%d'))
trans_time = str(time.strftime('%H%M%S'))

# Checksum calculation thanks to Eli Fulkerson
# https://www.elifulkerson.com/projects/librarystuff.php
def appendChecksum(text):
#calculate checksum
  check = 0
  for each in text:
    check = check + ord(each)
  check = check + ord('\0')
  check = (check ^ 0xFFFF) + 1

  checksum = "%4.4X" % (check)

  text = text + checksum

  return text

# Variables picked up from verifypatron.php
# Uncomment these to go live
librarycard = sys.argv[1] # Library card number
passpin = sys.argv[2] # Pass/PIN

# Variables for debugging. Use known good info
# librarycard = "29301001314159"
# passpin = "3141"


# Set up and generate login message

# SIP login credentials - change as needed
SIPuser = "" # Enter your SIP username here
SIPpass = "" # Enter your SIP password here
SIPBranch = "" # Enter your branch code (OrganizationID)


loginmsg = "9300CN" + SIPuser +"|CO" + SIPpass + "|CP" + SIPBranch + "|AY0AZ"
# Add checksum to login message
login = appendChecksum(loginmsg)

# Create patron verification string
PatVerifyMsg = "23001" + trans_date + "    " + trans_time + "AO|AA" + librarycard + "|AD" + passpin + "|AY1AZ"
PatronVerify = appendChecksum(PatVerifyMsg)
# print("Sending: " + PatronVerify)


# Server info
sipserver = "" # Change as needed, can be servername or IP
port = "" # SIP port number


# Log in to the SIP server
tn = telnetlib.Telnet(sipserver,port) # Change as needed
tn.write((login + "\r\n").encode('ascii'))
tn.read_until(b"941AY0AZFDFD") # Successful login - will add error handling later
 

# Verify patron
tn.write((PatronVerify + "\r\n").encode('ascii'))
time.sleep(2)
GetRetMsg = tn.read_very_eager().decode('ascii') # Read the response from the SIP server
RetMsg = str(GetRetMsg) # Prepare the response for further processing
print(RetMsg)

# Pull the name from the return string
CheckName = RetMsg[RetMsg.find("|AE")-1:RetMsg.find("|BL")]
NameString = (CheckName[4:])

# Check for valid patron
CheckValid = RetMsg[RetMsg.find("|BL")-1:RetMsg.find("|CQ")]
Valid = (CheckValid[4:])

# Check for valid password
CheckCred = RetMsg[RetMsg.find("|CQ")-1:RetMsg.find("|BH")]
Cred = (CheckCred[4:])

# Check for fines
CheckFines = RetMsg[RetMsg.find("|BV")-1:RetMsg.find("|PC")]
Fines = (CheckFines[4:])

# Pull screen message
CheckScreenMsg = RetMsg[RetMsg.find("|AF")-1:RetMsg.find("|AG")]
ScreenMsg = (CheckScreenMsg[4:])

# Pull print message
CheckPrintMsg = RetMsg[RetMsg.find("|AG")-1:RetMsg.find("|AY")]
PrintMsg = (CheckPrintMsg[4:])

# print("<h1>" + NameString + "</h1>")
# print("<p><strong>Valid Patron:</strong> " + Valid + "<br>")
# print("<p><strong>Valid PIN/Password:</strong> " + Cred + "<br>")
# print("<p><strong>Fines:</strong> $" + Fines + "<br>")
# print("<p><strong>Screen Message:</strong> " + ScreenMsg + "<br>")
# print("<p><strong>Print Message:</strong> " + PrintMsg + "<br>")

# Set temporary files for transporting patron information

temppatrontemplate = '/var/www/html/swagman/tempfiles/temppatrontemplate.php'
temppatronfile = '/var/www/html/swagman/tempfiles/temppatron.php'
tempitemstemplate = '/var/www/html/swagman/tempfiles/tempitemstemplate.html'
tempitemlist = '/var/www/html/swagman/tempfiles/tempitemlist.html'

# If temp files already exist, delete them and start fresh
if os.path.exists(temppatronfile):
  os.remove(temppatronfile)

if os.path.exists(tempitemlist):
  os.remove(tempitemlist)

# Copy the tempalte to create working file for patron data
shutil.copyfile(temppatrontemplate, temppatronfile)

# Copy the template to create a working file for items checked out
shutil.copyfile(tempitemstemplate, tempitemlist)


# Write SIP2 data to working file
with open(temppatronfile, "a") as tempfile:
  tempfile.write("$temppatronname = \"" + NameString + "\";\r\n")
  tempfile.write("$temppatronbarcode = \"" + librarycard + "\";\r\n")
  tempfile.write("$temppatronpasspin = \"" + passpin + "\";\r\n")
  tempfile.write("$tempfines = \"" + Fines + "\";\r\n")
  tempfile.write("?>")


tn.close() # Close the connection to the SIP server