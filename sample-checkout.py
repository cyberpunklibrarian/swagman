#!/usr/bin/env python3

import time
import sys
import telnetlib
import shutil

# Set up transaction date and time for checkin message
trans_date = str(time.strftime('%Y%m%d'))
trans_time = str(time.strftime('%H%M%S'))

# Checksum calculation thanks to Eli Fulkerson
# https://www.elifulkerson.com/projects/librarystuff.php
def appendChecksum(text):
#calculate checksum
  check = 0
  for each in text:
    check = check + ord(each)
  check = check + ord('\0')
  check = (check ^ 0xFFFF) + 1

  checksum = "%4.4X" % (check)

  text = text + checksum

  return text

# Variables picked up from verifypatron.php
# Uncomment these to go live.
librarycard = sys.argv[1] # Library card number
passpin = sys.argv[2] # Pass/PIN
itembarcode = sys.argv[3] # Item barcode

# Variables for debugging. Use known good info.
#librarycard = ""
#passpin = ""
#itembarcode = ""



# Set up and generate login message

# SIP login credentials - change as needed
SIPuser = "" # Enter your SIP username here
SIPpass = "" # Enter your SIP password here
SIPBranch = "" # Identify your branch code (OrganizationID)


loginmsg = "9300CN" + SIPuser +"|CO" + SIPpass + "|CP" + SIPBranch + "|AY0AZ"
# Add checksum to login message
login = appendChecksum(loginmsg)

# Create patron verification string
PatVerifyMsg = "23001" + trans_date + "    " + trans_time + "AO|AA" + librarycard + "|AD" + passpin + "|AY1AZ"
PatronVerify = appendChecksum(PatVerifyMsg)
# print("Sending: " + PatronVerify)

# Create a check out string
CheckoutMsgPrep = "11YN" + trans_date + "    " + trans_time + "                  " + "AO|AA" + librarycard + "|AB" + itembarcode + "|AC|AD" + passpin + "|BON|AY2AZ"
CheckoutMsg = appendChecksum(CheckoutMsgPrep)

# Server info
sipserver = "" # Change as needed, can be servername or IP
port = "" # SIP port number


# Log in to the SIP server
tn = telnetlib.Telnet(sipserver,port) # Change as needed
tn.write((login + "\r\n").encode('ascii'))
tn.read_until(b"941AY0AZFDFD") # Successful login - will add error handling later

time.sleep(1)

# Process the item check out
tn.write((CheckoutMsg + "\r\n").encode('ascii'))
time.sleep(2)
GetRetMsg = tn.read_very_eager().decode('ascii') # Read the response from the SIP server
RetMsg = str(GetRetMsg) # Prepare the response for further processing
#print(RetMsg)

# Pull the item barcode from the return string
GetBarcode = RetMsg[RetMsg.find("|AB")-1:RetMsg.find("|AJ")]
CheckoutBarcode = (GetBarcode[4:])

# Get the item title from the return string
GetTitle = RetMsg[RetMsg.find("|AJ")-1:RetMsg.find("|AH")]
ItemTitle = (GetTitle[4:])

# Get the due date from the return string
GetDue = RetMsg[RetMsg.find("|AH")-1:RetMsg.find("|BT")]
ItemDue = (GetDue[4:])

# Get the item type from the return string
GetType = RetMsg[RetMsg.find("|CH")-1:RetMsg.find("|AF")]
ItemType = (GetType[4:])



# Set up list of items checked out during this session
tempitemlist = '/var/www/html/swagman/tempfiles/tempitemlist.html'

with open(tempitemlist, "a") as tempitems:
  tempitems.write("<tr><td class=\"tg-0lax\">" + CheckoutBarcode + "</td><td class=\"tg-0lax\">" + ItemTitle + "</td><td class=\"tg-0lax\">" + ItemType + "</td><td class=\"tg-0lax\">" + ItemDue + "</td></tr>")
  #tempitems.write(CheckoutBarcode + " " + ItemTitle + " " + ItemType + " " + ItemDue + "<br>\r\n") 


tn.close()