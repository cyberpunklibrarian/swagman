<?php
include 'tempfiles/temppatron.php';

$displayname = explode(",", $temppatronname);
$lastname = $displayname[0];
$firstmiddlename = $displayname[1];
$patronfullname = $firstmiddlename." ".$lastname;

?>


<!DOCTYPE html>
<html>
<head>
<title>Check Out</title>
<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<link rel="stylesheet" type="text/css" href="css/userint.css" />
<script type="text/javascript" src="js/includepage.js"></script>
</head>
<body>
    <div id="wrapper">
        <div id="header">
            <!--<h1>Camarillo Public Library</h1>-->
            <img src = "images/CamarilloHeader.jpg">
        </div>
        <div id="contentliquid"><div id="content">

            <h1><?php echo $patronfullname; ?></h1>
            <p><span class="boldlabel">Library card:</span> <?php echo $temppatronbarcode; ?>
            <br><br>
            <span class="boldlabel">Fines owed:</span> $<?php echo $tempfines; ?></p>

            <form action="itemcheckout.php" method="post">
                
                <input type = "text" name="itembarcode" placeholder="Scan item barcode" autofocus>
                <input type = "submit" value="Check out">
                <br>
            </form>
            <br>
            <button class="finishbutton"  onclick="window.location.href = 'complete.php';">Finished Checking Out</button>

            <hr>

            <div w3-include-html="tempfiles/tempitemlist.html"></div> 

        

       </div></div>
        <div id="leftcolumn">

            <p><span class="coltop">At Your Library</span></p>  
            
            <img src="ads/FakeAd01.png" class="center"><br>

            <img src="ads/FakeAd02.png" class="center"><br>
        </div>

        <div id="rightcolumn">

            <p><span class="coltop">Check Out Times<br>and Overdues</span></p>
            <p>
                &bull; Books: 21 days<br><br>
                &bull; Sound Recordings: 21 days<br><br>
                &bull; Magazines: 21 days<br><br>
                &bull; Movies: 7 days<br><br>
            </p>

            <p>Items can be renewed one time provided no other patron has placed a hold on the item.</p>

            <hr>
            
            <p>Overdue fines for all items checked out of the Camarillo Public Library are $.20 per item per day with a maximum of $10.00 per item. Patrons owing $5 or more in charges will not be allowed to check out materials until their fine is paid.</p>
        </div>

        <div id="footer">
            <p>Swagman Self Checkout System &ndash; &#x24B8; 2020 &ndash; Library Systems & Services</p>
        </div>
    </div>

<script>
includeHTML();
</script> 

</body>
</html>
